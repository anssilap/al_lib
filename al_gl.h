#ifndef AL_GL
#define AL_GL

#if defined __linux__
#elif defined _WIN32
#include <gl/gl.h>
#endif
#include <al_debug.h>

// stb image used for texture loading
#ifndef _DEBUG
//#define STBI_NO_LINEAR
//#define STBI_NO_HDR
//#define STBI_ONLY_PNG
//#define STBI_ONLY_JPG
#endif
#ifndef STB_IMAGE_IMPLEMENTATION
#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>
#endif

#ifndef APIENTRY
#define APIENTRY
#endif
#ifndef APIENTRYP
#define APIENTRYP APIENTRY *
#endif
#ifndef GLAPI
#define GLAPI extern
#endif

typedef char GLchar;

// OpenGL defines
#define GL_ARRAY_BUFFER                             0x8892
#define GL_STATIC_DRAW                              0x88E4
#define GL_VERTEX_SHADER                            0x8B31
#define GL_FRAGMENT_SHADER                          0x8B30
#define GL_COMPILE_STATUS                           0x8B81
#define GL_LINK_STATUS                              0x8B82
#define GL_TEXTURE0                                 0x84C0
#define GL_TEXTURE1                                 0x84C1
#define GL_TEXTURE2                                 0x84C2
#define GL_TEXTURE3                                 0x84C3
#define GL_TEXTURE4                                 0x84C4
#define GL_TEXTURE5                                 0x84C5
#define GL_TEXTURE6                                 0x84C6
#define GL_TEXTURE7                                 0x84C7
#define GL_TEXTURE8                                 0x84C8
#define GL_TEXTURE9                                 0x84C9
#define GL_TEXTURE10                                0x84CA
#define GL_TEXTURE11                                0x84CB
#define GL_TEXTURE12                                0x84CC
#define GL_TEXTURE13                                0x84CD
#define GL_TEXTURE14                                0x84CE
#define GL_TEXTURE15                                0x84CF
#define GL_TEXTURE16                                0x84D0
#define GL_TEXTURE17                                0x84D1
#define GL_TEXTURE18                                0x84D2
#define GL_TEXTURE19                                0x84D3
#define GL_TEXTURE20                                0x84D4
#define GL_TEXTURE21                                0x84D5
#define GL_TEXTURE22                                0x84D6
#define GL_TEXTURE23                                0x84D7
#define GL_TEXTURE24                                0x84D8
#define GL_TEXTURE25                                0x84D9
#define GL_TEXTURE26                                0x84DA
#define GL_TEXTURE27                                0x84DB
#define GL_TEXTURE28                                0x84DC
#define GL_TEXTURE29                                0x84DD
#define GL_TEXTURE30                                0x84DE
#define GL_TEXTURE31                                0x84DF
#define GL_CLAMP_TO_BORDER                          0x812D
#define GL_PROGRAM_POINT_SIZE                       0x8642
#define GL_MAJOR_VERSION                            0x821B
#define GL_MINOR_VERSION                            0x821C
#define GL_SHADING_LANGUAGE_VERSION                 0x8B8C
#define GL_INVALID_FRAMEBUFFER_OPERATION            0x0506
#define GL_CURRENT_PROGRAM                          0x8B8D
#define GL_ACTIVE_TEXTURE                           0x84E0
#define GL_ARRAY_BUFFER_BINDING                     0x8894
#define GL_ELEMENT_ARRAY_BUFFER_BINDING             0x8895
#define GL_VERTEX_ARRAY_BINDING                     0x85B5
#define GL_BLEND_EQUATION_RGB                       0x8009
#define GL_BLEND_EQUATION_ALPHA                     0x883D
#define GL_FUNC_ADD                                 0x8006
#define GL_STREAM_DRAW                              0x88E0
#define GL_ELEMENT_ARRAY_BUFFER                     0x8893
#define GL_DYNAMIC_DRAW                             0x88E8
#define GL_MAX_TEXTURE_UNITS			            0x84E2

#define WGL_CONTEXT_DEBUG_BIT_ARB                   0x00000001
#define WGL_CONTEXT_FORWARD_COMPATIBLE_BIT_ARB      0x00000002
#define WGL_CONTEXT_MAJOR_VERSION_ARB               0x2091
#define WGL_CONTEXT_MINOR_VERSION_ARB               0x2092
#define WGL_CONTEXT_FLAGS_ARB                       0x2094

#define WGL_DRAW_TO_WINDOW_ARB                      0x2001
#define WGL_SUPPORT_OPENGL_ARB                      0x2010
#define WGL_DOUBLE_BUFFER_ARB                       0x2011
#define WGL_PIXEL_TYPE_ARB                          0x2013
#define WGL_COLOR_BITS_ARB                          0x2014
#define WGL_DEPTH_BITS_ARB                          0x2022
#define WGL_STENCIL_BITS_ARB                        0x2023
#define WGL_TYPE_RGBA_ARB                           0x202B

// OpenGL function prototypes
// return type, function name, parameters
#define AL_GL_LIST \
    /* 1.3 */ \
    GLE(void,   ActiveTexture,           GLenum texture) \
    /* 1.5 */ \
    GLE(void,   GenBuffers,              GLsizei n, GLuint *buffers) \
    GLE(void,   BindBuffer,              GLenum target, GLuint buffer) \
    GLE(void,   BufferData,              GLenum target, GLsizei size, const GLvoid *data, GLenum usage) \
    GLE(void,   DeleteBuffers,           GLsizei n, const GLuint* arrays) \
    /* 2.0 */ \
    GLE(void,   VertexAttribPointer,     GLuint index, GLint size, GLenum type, GLboolean normalized, GLsizei stride, const GLvoid *pointer) \
    GLE(void,   EnableVertexAttribArray, GLuint index) \
    GLE(void,   UseProgram,              GLuint program) \
    GLE(GLuint, CreateShader,            GLenum shadertype) \
    GLE(void,   ShaderSource,            GLuint shader, GLsizei count, const char **string, const GLint *length) \
    GLE(void,   CompileShader,           GLuint shader) \
    GLE(void,   GetShaderiv,             GLuint shader, GLenum pname, GLint *params) \
    GLE(void,   GetShaderInfoLog,        GLuint shader, GLsizei maxLength, GLsizei *length, char *infoLog) \
    GLE(GLuint, CreateProgram,           void) \
    GLE(void,   AttachShader,            GLuint program, GLuint shader) \
    GLE(void,   LinkProgram,             GLuint shader) \
    GLE(void,   GetProgramiv,            GLuint program, GLenum pname, GLint *params) \
    GLE(void,   GetProgramInfoLog,       GLuint program, GLsizei maxLength, GLsizei *length, char *infoLog) \
    GLE(void,   DeleteShader,            GLuint shader) \
    GLE(void,   Uniform1i,               GLint location, GLint v0) \
    GLE(GLint,  GetUniformLocation,      GLuint program, const GLchar *name) \
    GLE(void,   UniformMatrix4fv,        GLuint location, GLsizei count, GLboolean transpose, const GLfloat* value) \
    GLE(void,   Uniform4f,               GLuint location, GLfloat v0, GLfloat v1, GLfloat v2, GLfloat v3) \
    GLE(void,   Uniform3f,               GLuint location, GLfloat v0, GLfloat v1, GLfloat v2) \
    GLE(void,   Uniform2f,               GLuint location, GLfloat v0, GLfloat v1) \
    GLE(void,   Uniform1f,               GLuint location, GLfloat v0) \
    GLE(void,   BlendEquation,           GLenum mode) \
    GLE(void,   BlendEquationSeparate,   GLenum modeRGB, GLenum modeAlpha) \
    GLE(GLint,  GetAttribLocation,       GLuint program, const char* name) \
    /* 3.0 */ \
    GLE(void,   GenVertexArrays,         GLsizei n, GLuint *arrays) \
    GLE(void,   BindVertexArray,         GLuint array) \
    GLE(void,   DeleteVertexArrays,      GLsizei n, const GLuint* arrays) \

// Windows specific GL functions
#define AL_WGL_LIST \
    WGL(void,   SwapIntervalEXT,         GLuint enabled) \
    WGL(HGLRC,  CreateContextAttribsARB, HDC hdc, HGLRC share_context, const int *attrib_list) \
    /* end */

#define GLE(ret, name, ...) typedef ret APIENTRY name##proc(__VA_ARGS__); static name##proc * gl##name;
AL_GL_LIST
#undef GLE

#define WGL(ret, name, ...) typedef ret APIENTRY name##proc(__VA_ARGS__); static name##proc * wgl##name;
AL_WGL_LIST
#undef WGL

struct al_glInfo {
    b32 initialized;
    char* vendor;
    char* renderer;
    char* shading_language_version;
    i32 major_version;
    i32 minor_version;
    u32 vsync;
};

struct al_Texture
{
    GLuint id;
    GLuint width, height;
    GLuint bpp;
    //GLuint wrapS, wrapT;
    //GLuint filterMin, filterMax;
};

struct al_Shader
{
    bool initialized;
    GLuint id;
    char* vert_source_file;
    char* frag_source_file;
};

al_glInfo al_glGetInfo() {
    al_glInfo result = {0};
    result.vendor = (char*)glGetString(GL_VENDOR);
    result.renderer = (char*)glGetString(GL_RENDERER);

    glGetIntegerv(GL_MAJOR_VERSION, &result.major_version);
    glGetIntegerv(GL_MINOR_VERSION, &result.minor_version);
    result.shading_language_version = (char*)glGetString(GL_SHADING_LANGUAGE_VERSION);
    return result;
}

void* al_glGetFunc(const char* name)
{
    void *p = (void*)wglGetProcAddress(name);
    if (p == 0 || p == (void*)0x1 || (p == (void*)0x2) || (p == (void*)0x3) || (p == (void*)-1))
    {
        HMODULE module = LoadLibraryA("opengl32.dll");
        void* p = (void*)GetProcAddress(module, name);
    }
    return p;
}

//void* al_wglGetFunc(const char* name)
//{
//    void *p = (void*)wglGetProcAddress(name);
//    return p;
//}

void al_glLoadExtensions()
{
#define GLE(ret, name, ...) gl##name = (name##proc *) al_glGetFunc("gl" #name);
    AL_GL_LIST
#undef GLE

    // Platform specific functions
#if defined _WIN32
#define WGL(ret, name, ...) wgl##name = (name##proc *) al_glGetFunc("wgl" #name);
    AL_WGL_LIST
#undef WGL
#endif
}

al_Shader al_glShaderLoad(char* vert_source_file, char* frag_source_file)
{
    al_Shader shader;
    shader.initialized = false;

    al_Assert(al_FileExists(vert_source_file));
    al_Assert(al_FileExists(frag_source_file));

    const char* vertex_shader_source = (const char*)al_FileRead(vert_source_file, NULL);
    const char* fragment_shader_source = (const char*)al_FileRead(frag_source_file, NULL);

    GLint success;
    char info_log[512];

    // Vertex shader
    GLuint vertex_shader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertex_shader, 1, &vertex_shader_source, NULL);
    glCompileShader(vertex_shader);

    glGetShaderiv(vertex_shader, GL_COMPILE_STATUS, &success);
    if (!success)
    {
        glGetShaderInfoLog(vertex_shader, 512, NULL, info_log);
        al_PrintDebug(" ***** %s %s", vert_source_file, info_log);
        exit(1);
    }
    free((void*)vertex_shader_source);

    // Fragment shader
    GLuint fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragment_shader, 1, &fragment_shader_source, NULL);
    glCompileShader(fragment_shader);
    glGetShaderiv(fragment_shader, GL_COMPILE_STATUS, &success);
    if (!success)
    {
        glGetShaderInfoLog(fragment_shader, 512, NULL, info_log);
        al_PrintDebug(" ***** %s %s", frag_source_file, info_log);
        exit(1);
    }
    free((void*)fragment_shader_source);

    // Shader compilation
    shader.id = glCreateProgram();
    glAttachShader(shader.id, vertex_shader);
    glAttachShader(shader.id, fragment_shader);
    glLinkProgram(shader.id);

    glGetProgramiv(shader.id, GL_LINK_STATUS, &success);
    if (!success)
    {
        glGetProgramInfoLog(shader.id, 512, NULL, info_log);
        al_PrintDebug(info_log);
        exit(1);
    }
    glDeleteShader(vertex_shader);
    glDeleteShader(fragment_shader);
    shader.vert_source_file = vert_source_file;
    shader.frag_source_file = frag_source_file;
    shader.initialized = true;

    return shader;
}

void al_glShaderCompile(al_Shader* shader)
{
    shader->initialized = false;
    glDeleteShader(shader->id);
    GLint success;
    char info_log[512];

    al_Assert(al_FileExists(shader->vert_source_file));
    al_Assert(al_FileExists(shader->frag_source_file));

    const char* vertex_shader_source = (const char*)al_FileRead(shader->vert_source_file, NULL);
    const char* fragment_shader_source = (const char*)al_FileRead(shader->frag_source_file, NULL);
        
    // Vertex shader
    GLuint vertex_shader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertex_shader, 1, &vertex_shader_source, NULL);
    glCompileShader(vertex_shader);
    glGetShaderiv(vertex_shader, GL_COMPILE_STATUS, &success);
    if (!success)
    {
        glGetShaderInfoLog(vertex_shader, 512, NULL, info_log);
        al_PrintDebug(" ***** %s %s", shader->vert_source_file, info_log);
    }
    free((void*)vertex_shader_source);

    // Fragment shader
    GLuint fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragment_shader, 1, &fragment_shader_source, NULL);
    glCompileShader(fragment_shader);
    glGetShaderiv(fragment_shader, GL_COMPILE_STATUS, &success);
    if (!success)
    {
        glGetShaderInfoLog(fragment_shader, 512, NULL, info_log);
        al_PrintDebug(" ***** %s %s", shader->frag_source_file, info_log);
    }
    free((void*)fragment_shader_source);

    // Shader compilation
    shader->id = glCreateProgram();
    glAttachShader(shader->id, vertex_shader);
    glAttachShader(shader->id, fragment_shader);
    glLinkProgram(shader->id);

    glGetProgramiv(shader->id, GL_LINK_STATUS, &success);
    if (!success)
    {
        glGetProgramInfoLog(shader->id, 512, NULL, info_log);
        al_PrintDebug(info_log);
    }
    else
        al_PrintDebug("Shader recompile success\n");

    glDeleteShader(vertex_shader);
    glDeleteShader(fragment_shader);
    shader->initialized = true;
}

void al_glTextureLoad(al_Texture *tx, const char* filename,
    GLuint image_format = GL_RGB, GLuint internal_format = GL_RGB,
    GLuint wrap_s = GL_CLAMP_TO_BORDER, GLuint wrap_t = GL_CLAMP_TO_BORDER,
    GLuint filter_min = GL_LINEAR, GLuint filter_max = GL_LINEAR)
{
    //Texture tx;
    int width = 0, height = 0, bpp = 0;

    al_Assert(al_FileExists(filename));
    unsigned char* image_data = (unsigned char*)stbi_load(filename, &width, &height, &bpp, 0);
    al_Assert(image_data);

    tx->width = width;
    tx->height = height;
    tx->bpp = bpp;
    glGenTextures(1, &tx->id);
    glBindTexture(GL_TEXTURE_2D, tx->id);

    glTexImage2D(GL_TEXTURE_2D, 0, internal_format, width, height, 0, image_format, GL_UNSIGNED_BYTE, image_data);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, wrap_s);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, wrap_t);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, filter_min);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, filter_max);

    free(image_data);
    //return tx;
}

void al_glCheckErrors()
{
#ifdef DEBUG
    GLenum result = glGetError();
    if (result != GL_NO_ERROR)
    {
        switch (result) {
            case GL_INVALID_ENUM:
            al::PrintDebug("GL_INVALID_ENUM error\n");
            __debugbreak();
            break;
            case GL_INVALID_VALUE:
            al::PrintDebug("GL_INVALID_VALUE error\n");
            __debugbreak();
            break;
            case GL_INVALID_OPERATION:
            al::PrintDebug("GL_INVALID_OPERATION error\n");
            __debugbreak();
            break;
            case GL_INVALID_FRAMEBUFFER_OPERATION:
            al::PrintDebug("GL_INVALID_FRAMEBUFFER_OPERATION error\n");
            __debugbreak();
            break;
            case GL_OUT_OF_MEMORY:
            al::PrintDebug("GL_OUT_OF_MEMORY error\n");
            __debugbreak();
            break;
        }
    }
#endif
}

al_glInfo al_glInitWin32(al_Win32Data* win32, u32 vsync = 1, i32 major = 0, i32 minor = 0) {
    al_glInfo info = { 0 };
    PIXELFORMATDESCRIPTOR pfd =
    {
        sizeof(PIXELFORMATDESCRIPTOR),
        1,
        PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER,	//Flags
        PFD_TYPE_RGBA,			//The kind of framebuffer. RGBA or palette.
        32,						//Colordepth of the framebuffer.
        0, 0, 0, 0, 0, 0,
        0,
        0,
        0,
        0, 0, 0, 0,
        24,						//Number of bits for the depthbuffer
        8,						//Number of bits for the stencilbuffer
        0,						//Number of Aux buffers in the framebuffer.
        PFD_MAIN_PLANE,
        0,
        0, 0, 0
    };

    const int iPixelFormatAttribList[] =
    {
        WGL_DRAW_TO_WINDOW_ARB, GL_TRUE, 
        WGL_SUPPORT_OPENGL_ARB, GL_TRUE, 
        WGL_DOUBLE_BUFFER_ARB, GL_TRUE, 
        WGL_PIXEL_TYPE_ARB, WGL_TYPE_RGBA_ARB, 
        WGL_COLOR_BITS_ARB, 32, 
        WGL_DEPTH_BITS_ARB, 24, 
        WGL_STENCIL_BITS_ARB, 8, 
        0 // End of attributes list
    };

    int iContextAttribs[] = 
    {
        WGL_CONTEXT_MAJOR_VERSION_ARB, major, 
        WGL_CONTEXT_MINOR_VERSION_ARB, minor, 
        WGL_CONTEXT_FLAGS_ARB, WGL_CONTEXT_FORWARD_COMPATIBLE_BIT_ARB, 
        0 // End of attributes list
    };

    int pixel_format;
    pixel_format = ChoosePixelFormat(win32->dc, &pfd);
    SetPixelFormat(win32->dc, pixel_format, &pfd);

    win32->opengl_context = wglCreateContext(win32->dc);
    wglMakeCurrent(win32->dc, win32->opengl_context);
    al_glLoadExtensions();

    if (wglCreateContextAttribsARB != NULL && major >= 3) {
        wglMakeCurrent(NULL, NULL);
        wglDeleteContext(win32->opengl_context);
        win32->opengl_context = wglCreateContextAttribsARB(win32->dc, 0, iContextAttribs);
        wglMakeCurrent(win32->dc, win32->opengl_context);
    }

    al_Assert(win32->opengl_context);

    info = al_glGetInfo();
    al_PrintDebug("%s %s OpenGL %i.%i\n", info.vendor, info.renderer, info.major_version, info.minor_version, major, minor);

    //if (info.major_version < major || (info.major_version == major && info.minor_version < minor))
    //{
    //    al_PrintDebug("OpenGL version %d.%d or higher required.");
    //}

    info.vsync = vsync;
    wglSwapIntervalEXT(vsync);

    info.initialized = true;
    return info;
}
#endif // AL_GL
