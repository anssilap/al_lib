#ifndef AL_STRING
#define AL_STRING

//#include <stdlib.h>
//#include <stdio.h>
#include <string.h>
//#include <assert.h>
//#include <stdarg.h>
//#define MESSAGE_SIZE 4096

#include <al_lib.h>
#include <al_math.h>

namespace al {
}
//struct al_string
//{
//	size_t length;
//	size_t allocated;
//	char* c_str;
//
//	al_string()
//	{
//		length = 0;
//		allocated = 0;
//		c_str = nullptr;
//	}
//
//	al_string(const char* str)
//	{
//		length = 0;
//		allocated = 0;
//		c_str = nullptr;
//
//		length = len(str);
//		if (allocated < length + 1)
//		{
//			c_str = (char*)malloc((length + 1));
//			memset(c_str, 0, length + 1);
//			allocated = length + 1;
//		}
//		memcpy(c_str, str, length);
//		// #NOTE Already null-terminated because of memset(length+1)
//	}
//
//	void clear()
//	{
//		free(c_str);
//		length = allocated = 0;
//		c_str = nullptr;
//	}
//
//	void operator=(const char* str)
//	{
//		length = len(str);
//		if (allocated < length + 1)
//		{
//			c_str = (char*)malloc((length + 1));
//			memset(c_str, 0, length + 1);
//			allocated = length + 1;
//		}
//		memcpy(c_str, str, length);
//		//c_str[length] = '\0';
//	}
//
//	void format(const char* format, ...)
//	{
//		va_list v;
//		va_start(v, format);
//		char buffer[MESSAGE_SIZE] = "";
//		vsprintf_s(buffer, MESSAGE_SIZE, format, v);
//		append(buffer);
//		va_end(v);
//	}
//
//	void append_endl()
//	{
//		// #TODO Other platforms
//		append("\r\n");
//	}
//
//	//////////////////////////////////////////////////////////////////////////
//	// Concatenation overloads
//	//////////////////////////////////////////////////////////////////////////
//	void append(const char* str)
//	{
//		size_t new_str_length = len(str);
//		size_t total_length = new_str_length + length;
//		if (allocated < total_length + 1)
//		{
//			char* oldptr = c_str;
//			c_str = (char*)malloc((total_length + 1));
//			assert(c_str != nullptr);
//			memset(c_str, 0, total_length + 1);
//			allocated = total_length + 1;
//			memcpy(c_str, oldptr, length);
//			free(oldptr);
//			oldptr = nullptr;
//		}
//		memcpy(c_str + length, str, new_str_length);
//		length = total_length;
//		c_str[length] = '\0';
//	}
//
//	void append(float number, char* format = nullptr) {
//		char str[20];
//		if (format == nullptr)
//			sprintf_s(str, "%.4f", number);
//		else
//			sprintf_s(str, format, number);
//
//		size_t new_str_length = len(str);
//		size_t total_length = new_str_length + length;
//		if (allocated < total_length + 1)
//		{
//			char* oldptr = c_str;
//			c_str = (char*)malloc((total_length + 1));
//			assert(c_str != nullptr);
//			memset(c_str, 0, total_length + 1);
//			allocated = total_length + 1;
//			memcpy(c_str, oldptr, length);
//			free(oldptr);
//			oldptr = nullptr;
//		}
//		memcpy(c_str + length, str, new_str_length);
//		length = total_length;
//		c_str[length] = '\0';
//	}
//
//	void append(long number)
//	{
//		char str[20];
//		_itoa_s(number, str, 20, 10);
//
//		size_t new_str_length = len(str);
//		size_t total_length = new_str_length + length;
//		if (allocated < total_length + 1)
//		{
//			char* oldptr = c_str;
//			c_str = (char*)malloc((total_length + 1));
//			assert(c_str != nullptr);
//			memset(c_str, 0, total_length + 1);
//			allocated = total_length + 1;
//			memcpy(c_str, oldptr, length);
//			free(oldptr);
//			oldptr = nullptr;
//		}
//		memcpy(c_str + length, str, new_str_length);
//		length = total_length;
//		c_str[length] = '\0';
//	}
//
//
//	void append(int number)
//	{
//		char str[20];
//		_itoa_s(number, str, 20, 10);
//		size_t new_str_length = len(str);
//		size_t total_length = new_str_length + length;
//		if (allocated < total_length + 1)
//		{
//			char* oldptr = c_str;
//			c_str = (char*)malloc((total_length + 1));
//			assert(c_str != nullptr);
//			memset(c_str, 0, total_length + 1);
//			allocated = total_length + 1;
//			memcpy(c_str, oldptr, length);
//			free(oldptr);
//			oldptr = nullptr;
//		}
//		memcpy(c_str + length, str, new_str_length);
//		length = total_length;
//		c_str[length] = '\0';
//	}
//private:
//	size_t len(const char* str)
//	{
//		size_t count = 0;
//		while (*str++)
//		{
//			++count;
//		}
//		return count;
//	}
//};
#endif //AL_STRING
