#pragma once
#ifndef AL_DEBUG_H
#include <stdarg.h>
#include <stdio.h>
#ifdef _MSC_VER
// #TODO Get rid of this if you can and just do a extern?
#include <windows.h>
#endif
void al_PrintDebug(const char* format, ...);

struct Timer
{
    unsigned long long initial_ticks;
    unsigned long long ticks_per_second;

    unsigned long long ticks;
    unsigned long long delta_ticks;

    unsigned long long nanoseconds;
    unsigned long long microseconds;
    unsigned long long milliseconds;
    double seconds;

    unsigned long long delta_nanoseconds;
    unsigned long long delta_microseconds;
    unsigned long long delta_milliseconds;
    double delta_seconds;
};
#endif // AL_DEBUG_H

#ifdef AL_DEBUG_IMPLEMENTATION
#undef AL_DEBUG_IMPLEMENTATION

void al_PrintDebug(const char* format, ...)
{
#ifdef AL_DEBUG
    va_list v;
    va_start(v, format);
#ifdef _MSC_VER
    char buffer[512];
    _vsnprintf_s(buffer, sizeof(buffer) / sizeof(char), format, v);
    vprintf(format, v);
#ifdef UNICODE
    BSTR output;
    int length = lstrlenA(buffer);
    int length_w = MultiByteToWideChar(CP_ACP, 0, buffer, length, 0, 0);
    if (length_w > 0)
    {
        output = SysAllocStringLen(0, length_w);
        MultiByteToWideChar(CP_ACP, 0, buffer, length, output, length_w);
        OutputDebugString(output);
        SysFreeString(output);
    }
#else
    OutputDebugString(buffer);
#endif
#else
    if (format != NULL)
        vprintf(format, v);
#endif
#endif
}

#ifdef AL_DEBUG
#define al_Assert(x) if (!(x)) { al_PrintDebug("     *** Assertion failed: (%s) at %s line %i\n", #x, __FILE__, __LINE__); __debugbreak(); }
#else
#define al_Assert 
#endif

#endif
