#ifndef AL_AUDIO
#define AL_AUDIO

//#include <xaudio2.h>
//#include <dsound.h>
//#include <soloud.h>
//#include <soloud_thread.h>
//#include <soloud_wav.h>
//
//#if defined __linux__
//#elif defined _WIN32
//#define WITH_XAUDIO2
//#endif

enum {
	MAX_SOUND_SAMPLES = 1024 * 1024
};

struct al_Audio {
	u32 sounds_samples_per_second;
	i16* sounds_samples;
	i16 sound_sample_buffer[MAX_SOUND_SAMPLES];
};

#endif // AL_AUDIO