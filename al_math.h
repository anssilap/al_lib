#ifndef AL_MATH
#define AL_MATH

#include <al_lib.h>

// #TODO Check how feasible it would be to get rid of math.h
#include <math.h>

#define KiloBytes(Value) ((Value)*1024)
#define MegaBytes(Value) (KiloBytes(Value)*1024)
#define GigaBytes(Value) (MegaBytes(Value)*1024)

#define PI 3.14159265359f

float al_Lerp(float a, float b, float t);
float al_Round(float n);
float al_Median(float a, float b, float c);
float al_Clamp(float number, float low, float high);
float al_ToRadians(float degrees);

struct al_Vec2f
{
    float x;
    float y;
};

struct al_Vec3f
{
    float x;
    float y;
    float z;
};
al_Vec3f V3f(float xx, float yy, float zz);

al_Vec3f al_Vec3fAdd(al_Vec3f v1, al_Vec3f v2);
al_Vec3f al_Vec3fSub(al_Vec3f v1, al_Vec3f v2);
al_Vec3f al_Vec3fMul(float scalar);
al_Vec3f Normalize(al_Vec3f v1);
float al_Vec3f_Length(al_Vec3f v1);
al_Vec3f al_Vec3f_Cross(al_Vec3f v1, al_Vec3f v2);
float al_Vec3f_Dot(al_Vec3f v1, al_Vec3f v2);

struct al_Vec4f
{
    f32 x;
    f32 y;
    f32 z;
    f32 w;
};
al_Vec4f al_V4f(float x, float y, float z, float w);

struct al_Vec2i
{
    i32 x;
    i32 y;
};
al_Vec2i al_V2i(int x, int y);

struct al_Vec3i
{
    i32 x;
    i32 y;
    i32 z;
};
al_Vec3i al_V3i(int x, int y, int z);

struct al_Vec4i
{
    i32 x;
    i32 y;
    i32 z;
    i32 w;
};
al_Vec4i al_V4i(int x, int y, int z, int w);

// Matrices are in columns first format i.e.
// [ 0] [ 4] [ 7] [12]
// [ 1] [ 5] [ 9] [13]
// [ 2] [ 6] [10] [14]
// [ 3] [ 7] [11] [15]
struct al_Mat4
{
    float &operator[](const int i);
private:
    f32 elements[16];
};

al_Mat4 al_Identity();
al_Mat4 al_Orthographic(f32 left, f32 right, f32 bottom, f32 top, f32 nearplane, f32 farplane);
al_Mat4 al_Perspective(float fov_y, float aspect_ratio, float nearplane, float farplane);
al_Mat4 al_Mat4_LookAt(al_Vec3f eye, al_Vec3f center, al_Vec3f up);
al_Mat4 al_Mat4_Rotate(al_Mat4 m, float angle, al_Vec3f rotation);
al_Mat4 al_Mat4_Translate(al_Mat4 m, al_Vec3f translation);
al_Mat4 al_Mat4_Add(al_Mat4 m1, al_Mat4 m2);
al_Mat4 al_Mat4_Mul(al_Mat4 m1, al_Mat4 m2);

struct al_Mat3
{
    f32 elements[9];
    float &operator[](const int i);
};

struct al_Quaternion
{
    float x, y, z, w;
};

float al_Perlin2d(int seed, float x, float y, float freq, int depth);

struct al_Rng {
    u64 state;
    u64 inc;
};

u32 al_RandomUint32(al_Rng* rnd);
void al_RandomSeed(al_Rng* rnd, u64 initstate, u64 initseq);
void al_RandomSeed(al_Rng* rnd);
u32 al_RandomRange(al_Rng* rnd, u32 n0, u32 n1);
#endif // AL_MATH

// 
// **************************
// ***** Implementation *****
// **************************
// 
#ifdef AL_MATH_IMPLEMENTATION
#undef AL_MATH_IMPLEMENTATION
float al_Lerp(float a, float b, float t)
{
    return (1 - t) * a + t * b;
}

float al_Round(float n)
{
    return (float)floor(n+0.5f);
}

float al_Median(float a, float b, float c)
{
    return max(min(a, b), min(max(a, b), c));
}

float al_Clamp(float number, float low, float high)
{
    return number >= low && number <= high ? number : number < low ? low : high;
}

float al_ToRadians(float degrees)
{
    float result = 0;
    result = degrees * (PI / 180);
    return result;
}

al_Vec3f al_Vec3f_Add(al_Vec3f v1, al_Vec3f v2)
{
    v1.x += v2.x;
    v1.y += v2.y;
    v1.z += v2.z;
    return v1;
}

al_Vec3f al_Vec3f_Sub(al_Vec3f v1, al_Vec3f v2)
{
    v1.x -= v2.x;
    v1.y -= v2.y;
    v1.z -= v2.z;
    return v1;
}

al_Vec3f al_Vec3f_Mul(al_Vec3f v1, float scalar)
{
    v1.x *= scalar;
    v1.y *= scalar;
    v1.z *= scalar;
    return v1;
}

al_Vec3f al_Vec3f_Normalize(al_Vec3f v1)
{
    al_Vec3f result;
    result.x = v1.x;
    result.y = v1.y;
    result.z = v1.z;
    float l = al_Vec3f_Length(v1);
    result.x /= l;
    result.y /= l;
    result.z /= l;
    return result;
}

al_Vec3f al_Vec3f_Cross(al_Vec3f v1, al_Vec3f v2)
{
    al_Vec3f result = {0};
    result.x = (v1.y * v2.z) - (v1.z * v2.y);
    result.y = (v1.z * v2.x) - (v1.x * v2.z);
    result.z = (v1.x * v2.y) - (v1.y * v2.x);
    return result;
}

float al_Vec3f_Dot(al_Vec3f v1, al_Vec3f v2)
{
    float result;
    result = (v1.x * v2.x) + (v1.y * v2.y) + (v1.z * v2.z);
    return result;
}

float al_Vec3f_Length(al_Vec3f v1)
{
    float result;
    result = (float)sqrt(pow(v1.x, 2) + pow(v1.y, 2) + pow(v1.z, 2));
    return result;
}

al_Vec3f al_V3f(float x, float y, float z)
{
    al_Vec3f result = { x, y, z };
    return result;
}

al_Vec4f al_V4f(float x, float y, float z, float w)
{
    al_Vec4f result = { x, y, z, w };
    return result;
}

al_Vec2i al_V2i(int x, int y)
{
    al_Vec2i result = { x, y };
    return result;
}

al_Vec3i al_V3i(int x, int y, int z)
{
    al_Vec3i result = { x, y, z };
    return result;
}

al_Vec4i al_V4i(int x, int y, int z, int w)
{
    al_Vec4i result = { x, y, z, w };
    return result;
}

float& al_Mat4::operator[](const int i)
{
    return elements[i];
}

al_Mat4 al_Identity()
{
    al_Mat4 result;
    result[0] = 1.0f;
    result[1] = 0.0f;
    result[2] = 0.0f;
    result[3] = 0.0f;

    result[4] = 0.0f;
    result[5] = 1.0f;
    result[6] = 0.0f;
    result[7] = 0.0f;

    result[8] = 0.0f;
    result[9] = 0.0f;
    result[10] = 1.0f;
    result[11] = 0.0f;

    result[12] = 0.0f;
    result[13] = 0.0f;
    result[14] = 0.0f;
    result[15] = 1.0f;
    return result;
}

al_Mat4 al_Orthographic(f32 left, f32 right, f32 bottom, f32 top, f32 nearplane, f32 farplane)
{
    al_Mat4 result = al_Identity();

    result[0] = 2.0f / (right - left);
    result[5] = 2.0f / (top - bottom);
    result[10] = -2.0f / (farplane - nearplane);
    result[12] = -((right + left) / (right - left));
    result[13] = -((top + bottom) / (top - bottom));
    result[14] = -((farplane + nearplane) / (farplane - nearplane));
    result[15] = 1.0f;

    return result;
}

al_Mat4 al_Perspective(float fov_y, float aspect_ratio, float nearplane, float farplane)
{
    al_Mat4 result = al_Identity();
    float tan_half_fovy = tanf(0.5f * fov_y);

    result[0] = 1.0f / (aspect_ratio * tan_half_fovy);
    result[5] = 1.0 / tan_half_fovy;
    result[11] = -1.0f;
    result[10] = -(farplane + nearplane) / (farplane - nearplane);
    result[14] = -2.0f * farplane * nearplane / (farplane - nearplane);

    return result;
}

float Perlin2d(int seed, float x, float y, float freq, int depth)
{
    static int hash[] = { 208,34,231,213,32,248,233,56,161,78,24,140,71,48,140,254,245,255,247,247,40,
        185,248,251,245,28,124,204,204,76,36,1,107,28,234,163,202,224,245,128,167,204,
        9,92,217,54,239,174,173,102,193,189,190,121,100,108,167,44,43,77,180,204,8,81,
        70,223,11,38,24,254,210,210,177,32,81,195,243,125,8,169,112,32,97,53,195,13,
        203,9,47,104,125,117,114,124,165,203,181,235,193,206,70,180,174,0,167,181,41,
        164,30,116,127,198,245,146,87,224,149,206,57,4,192,210,65,210,129,240,178,105,
        228,108,245,148,140,40,35,195,38,58,65,207,215,253,65,85,208,76,62,3,237,55,89,
        232,50,217,64,244,157,199,121,252,90,17,212,203,149,152,140,187,234,177,73,174,
        193,100,192,143,97,53,145,135,19,103,13,90,135,151,199,91,239,247,33,39,145,
        101,120,99,3,186,86,99,41,237,203,111,79,220,135,158,42,30,154,120,67,87,167,
        135,176,183,191,253,115,184,21,233,58,129,233,142,39,128,211,118,137,139,255,
        114,20,218,113,154,27,127,246,250,1,8,198,250,209,92,222,173,21,88,102,219 };

    float xa = x * freq;
    float ya = y * freq;
    float amp = 1.0;
    float fin = 0;
    float div = 0.0;

    for (int i = 0; i < depth; i++)
    {
        div += 256 * amp;

        int x_int = (int)xa;
        int y_int = (int)ya;
        float x_frac = xa - x_int;
        float y_frac = ya - y_int;
        int s = hash[(hash[(y_int + seed) % 256] + x_int) % 256];
        int t = hash[(hash[(y_int + seed) % 256] + x_int + 1) % 256];
        int u = hash[(hash[(y_int + 1 + seed) % 256] + x_int) % 256];
        int v = hash[(hash[(y_int + 1 + seed) % 256] + x_int + 1) % 256];
        float low = s + (x_frac * x_frac * (3 - 2 * x_frac)) * (t - s);
        float high = u + (x_frac * x_frac * (3 - 2 * x_frac)) * (v - u);
        float n2dr = low + (y_frac * y_frac * (3 - 2 * y_frac)) * (high - low);

        fin += n2dr * amp;
        amp /= 2;
        xa *= 2;
        ya *= 2;
    }
    return fin / div;
}

al_Mat4 al_Mat4_Add(al_Mat4 m1, al_Mat4 m2)
{
    al_Mat4 result = al_Identity();
    for (int i = 0; i < 16; i++)
    {
        result[i] = m1[i] + m2[i];
    }
    return result;
}

al_Mat4 al_Mat4_Mul(al_Mat4 m1, al_Mat4 m2)
{
    // Matrices are in columns first format i.e.
    // [ 0] [ 4] [ 8] [12]
    // [ 1] [ 5] [ 9] [13]
    // [ 2] [ 6] [10] [14]
    // [ 3] [ 7] [11] [15]
    al_Mat4 result = al_Identity();
    for (int columns = 0; columns < 4; ++columns) {
        for (int rows = 0; rows < 4; ++rows) {
            float sum = 0;
            for (int current_m = 0; current_m < 4; ++current_m) {
                sum += m1[current_m*4+rows] * m2[columns*4+current_m];
            }
            result[columns*4+rows] = sum;
        }
    }
    return result;
}

al_Mat4 al_Mat4_LookAt(al_Vec3f position, al_Vec3f target, al_Vec3f world_up)
{
    al_Mat4 result;

    al_Vec3f f = al_Vec3f_Normalize(al_Vec3f_Sub(position, target));
    al_Vec3f s = al_Vec3f_Normalize(al_Vec3f_Cross(al_Vec3f_Normalize(world_up), f));
    al_Vec3f u = al_Vec3f_Cross(f, s);

    al_Mat4 translation = al_Identity();
    translation[12] = -position.x;
    translation[13] = -position.y;
    translation[14] = -position.z;
    al_Mat4 rotation = al_Identity();
    rotation[0] = s.x;
    rotation[4] = s.y;
    rotation[8] = s.z;
    
    rotation[1] = u.x;
    rotation[5] = u.y;
    rotation[9] = u.z;

    rotation[2] = f.x;
    rotation[6] = f.y;
    rotation[10] = f.z;

    result = al_Mat4_Mul(rotation, translation);
    return result;

    //result[0] = s.x;
    //result[1] = u.x;
    //result[2] = -f.x;

    //result[4] = s.y;
    //result[5] = u.y;
    //result[6] = -f.y;

    //result[8] = s.z;
    //result[9] = u.z;
    //result[10] = -f.z;

    //result[0] = s.x;
    //result[1] = s.y;
    //result[2] = s.z;
    //
    //result[4] = u.x;
    //result[5] = u.y;
    //result[6] = u.z;
    //
    //result[8] = -f.z;
    //result[9] = -f.y;
    //result[10] = -f.z;

    //result[12] = -al_Vec3f_Dot(s, eye);
    //result[13] = -al_Vec3f_Dot(u, eye);
    //result[14] = al_Vec3f_Dot(f, eye);
    //result[15] = 1.0f;

    // Matrices are in columns first format i.e.
    // [ 0] [ 4] [ 8] [12]
    // [ 1] [ 5] [ 9] [13]
    // [ 2] [ 6] [10] [14]
    // [ 3] [ 7] [11] [15]
}

al_Mat4 al_Mat4_Rotate(al_Mat4 m, float angle, al_Vec3f rotation)
{
    rotation = al_Vec3f_Normalize(rotation);
    float length = al_Vec3f_Length(rotation);

    float sin_theta = sin(al_ToRadians(angle));
    float cos_theta = cos(al_ToRadians(angle));
    float cos_value = 1.0f - cos_theta;

    m[0] = (rotation.x * rotation.x * cos_value) + cos_theta;
    m[1] = (rotation.x * rotation.y * cos_value) + (rotation.z * sin_theta);
    m[2] = (rotation.x * rotation.z * cos_value) - (rotation.y * sin_theta);
    m[4] = (rotation.y * rotation.x * cos_value) - (rotation.z * sin_theta);
    m[5] = (rotation.y * rotation.y * cos_value) + cos_theta;
    m[6] = (rotation.y * rotation.z * cos_value) + (rotation.x * sin_theta);
    m[8] = (rotation.z * rotation.x * cos_value) + (rotation.y * sin_theta);
    m[9] = (rotation.z * rotation.y * cos_value) - (rotation.x * sin_theta);
    m[10] = (rotation.z * rotation.z * cos_value) + cos_theta;
        
    return m;
}

al_Mat4 al_Mat4_Translate(al_Mat4 m, al_Vec3f translation)
{
    m[12] = translation.x;
    m[13] = translation.y;
    m[14] = translation.z;
    return m;
}

u32 al_RandomUint32(al_Rng* rnd)
{
    u64 oldstate = rnd->state;
    rnd->state = oldstate * 6364136223846793005ULL + rnd->inc;
    u32 xorshifted = ((oldstate >> 18u) ^ oldstate) >> 27u;
    u32 rot = oldstate >> 59u;
#pragma warning(disable:4146)
    return (xorshifted >> rot) | (xorshifted << ((-rot) & 31));
#pragma warning(default:4146)
}

void al_RandomSeed(al_Rng* rnd, u64 initstate, u64 initseq)
{
    rnd->state = 0U;
    rnd->inc = (initseq << 1u) | 1u;
    al_RandomUint32(rnd);
    rnd->state += initstate;
    al_RandomUint32(rnd);
}

void al_RandomSeed(al_Rng* rnd)
{
    al_RandomSeed(rnd, time(NULL), (intptr_t)&rnd);
}

u32 al_RandomRange(al_Rng* rnd, u32 n0, u32 n1)
{
    if (n0 == n1)
        return n0;
    u32 bound;

    if (n0 > n1)
    {
        int n2 = n0;
        n0 = n1;
        n1 = n2;
    }

    bound = n1 - n0 + 1;
#pragma warning(disable:4146)
    u32 threshold = -bound % bound;
#pragma warning(default:4146)

    for (;;)
    {
        u32 r = al_RandomUint32(rnd);
        if (r >= threshold)
            return (r % bound + n0);
    }
}
#endif // AL_MATH_IMPLEMENTATION
