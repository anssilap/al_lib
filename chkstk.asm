.code

;rax contains size to check is commited
__chkstk proc
;move stack pointer and save registers
   sub         rsp, 10h
   mov         qword ptr [rsp], r10  
   mov         qword ptr [rsp + 8h], r11  
   xor         r11, r11  
;get stack position after this call returns, check size from here
   lea         r10, [rsp + 18h]  
;calculate address that must be commited up to
   sub         r10, rax  
;set 0 if address is negative
   cmovb       r10, r11  
;get the stack limit
   mov         r11, qword ptr gs:[10h]  
;check if stack limit is already below target
   cmp         r10, r11  
   jae		   done                              ; here
;get the page of the target address
   and         r10w, 0F000h
force_fault_commit:
;set first byte on next page to fault
;on fault the fault handler will recognize the stack address and commit pages
   lea         r11, [r11 - 1000h]  
   mov         byte ptr [r11], 0h  
   cmp         r10, r11  
   jne     force_fault_commit                      ; here
done:
;retrieve previous register value and reset stack pointer
   mov         r10, qword ptr [rsp]  
   mov         r11, qword ptr [rsp + 8h]  
   add         rsp, 10h
   ret                                            ; here
__chkstk endp

end
