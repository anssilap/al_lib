#pragma once
#ifndef AL_IMGUI
#define AL_IMGUI

#include <imgui.h>
#include <game_data.h>

int g_AttribLocationTex = 0;

struct game_data;

struct ed_debug
{
	GLuint vao_debug;
	GLuint vbo_debug;
	GLuint ebo_debug;
	al::texture font_texture;
	al::shader shader;
};

void debug_init(al::ed_debug* dbg, al::game_data* game)
{
	ImGuiIO& io = ImGui::GetIO();
	io.KeyMap[ImGuiKey_Delete] = VK_DELETE;

	//io.RenderDrawListsFn = imgui_render_drawLists;

	io.ImeWindowHandle = game->win32.window;

	ImVec4 clear_color = ImColor(114, 144, 154);

	// dear imgui
	GLint last_texture, last_array_buffer, last_vertex_array;
	glGetIntegerv(GL_TEXTURE_BINDING_2D, &last_texture);
	glGetIntegerv(GL_ARRAY_BUFFER_BINDING, &last_array_buffer);
	glGetIntegerv(GL_VERTEX_ARRAY_BINDING, &last_vertex_array);
	g_AttribLocationTex = glGetUniformLocation(dbg->shader.id, "tex");
	int g_AttribLocationProjMtx = glGetUniformLocation(dbg->shader.id, "projection");
	int g_AttribLocationPosition = glGetAttribLocation(dbg->shader.id, "position");
	int g_AttribLocationUV = glGetAttribLocation(dbg->shader.id, "uv");
	int g_AttribLocationColor = glGetAttribLocation(dbg->shader.id, "color");

	glGenBuffers(1, &dbg->vbo_debug);
	glGenBuffers(1, &dbg->ebo_debug);
	glGenVertexArrays(1, &dbg->vao_debug);
	glBindVertexArray(dbg->vao_debug);

	glBindBuffer(GL_ARRAY_BUFFER, dbg->vbo_debug);

	glEnableVertexAttribArray(g_AttribLocationPosition);
	glEnableVertexAttribArray(g_AttribLocationUV);
	glEnableVertexAttribArray(g_AttribLocationColor);

#define OFFSETOF(TYPE, ELEMENT) ((size_t)&(((TYPE *)0)->ELEMENT))
	glVertexAttribPointer(g_AttribLocationPosition, 2, GL_FLOAT, GL_FALSE, sizeof(ImDrawVert), (GLvoid*)OFFSETOF(ImDrawVert, pos));
	glVertexAttribPointer(g_AttribLocationUV, 2, GL_FLOAT, GL_FALSE, sizeof(ImDrawVert), (GLvoid*)OFFSETOF(ImDrawVert, uv));
	glVertexAttribPointer(g_AttribLocationColor, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(ImDrawVert), (GLvoid*)OFFSETOF(ImDrawVert, col));
#undef OFFSETOF

	imgui_create_fonts_texture(dbg);

	glBindTexture(GL_TEXTURE_2D, last_texture);
	glBindBuffer(GL_ARRAY_BUFFER, last_array_buffer);
	glBindVertexArray(last_vertex_array);
}

bool imgui_create_fonts_texture(al::ed_debug* dbg)
{
	// Build texture atlas
	ImGuiIO& io = ImGui::GetIO();
	unsigned char* pixels;
	int width, height;
	io.Fonts->GetTexDataAsRGBA32(&pixels, &width, &height);   // Load as RGBA 32-bits for OpenGL3 demo because it is more likely to be compatible with user's existing shader.

	// Upload texture to graphics system
	GLint last_texture;
	glGetIntegerv(GL_TEXTURE_BINDING_2D, &last_texture);
	glGenTextures(1, &dbg->font_texture.id);
	glBindTexture(GL_TEXTURE_2D, dbg->font_texture.id);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels);

	// Store our identifier
	io.Fonts->TexID = (void *)(intptr_t)dbg->font_texture.id;

	// Restore state
	glBindTexture(GL_TEXTURE_2D, last_texture);

	return true;
}

void imgui_render(al::ed_debug* dbg, al::game_data* game)
{
	ImGuiIO& io = ImGui::GetIO();
	int w, h;
	int display_w, display_h;
	w = game->window_area.x;
	h = game->window_area.y;
	display_w = game->client_area.x;
	display_h = game->client_area.y;
	io.DisplaySize = ImVec2((float)w, (float)h);
	io.DisplayFramebufferScale = ImVec2(w > 0 ? ((float)display_w / w) : 0,
		h > 0 ? ((float)display_h / h) : 0);
	io.DeltaTime = game->time.delta_seconds;
	io.MousePos = ImVec2(game->input.mouse.position.x, game->input.mouse.position.y);
	io.MouseDown[0] = game->input.mouse.left_button.down != 0;
	io.MouseDown[1] = game->input.mouse.right_button.down != 0;
	io.MouseWheel = (float)game->input.mouse.delta_wheel;

	bool show = true;
	ImGui::NewFrame();

	ImGui::SetNextWindowSize(ImVec2(300, (float)game->client_area.y));
	ImGui::SetNextWindowPos(ImVec2(0, 0));
	ImGui::Begin("Debug information");
	ImGui::Text("frame time: %.3f ms (%.1f FPS)", game->time.delta_seconds * 1000.0f, 1.0f / game->time.delta_seconds);
	ImGui::Text("camera position: %.2f, %.2f", game->camera.world_location.x, game->camera.world_location.y);
	ImGui::Text("");
	ImGui::Text("mouse screen pos: %i, %i", game->input.mouse.screen_position.x, game->input.mouse.screen_position.y);
	ImGui::Text("mouse window pos: %i, %i", game->input.mouse.position.x, game->input.mouse.position.y);
	ImGui::Text("mouse buttons down: %s%s",
		game->input.mouse.left_button.down ? "lmb " : "",
		game->input.mouse.right_button.down ? "rmb " : "");
	ImGui::Text("");
	ImGui::Text("gamepad 0 status:");
	ImGui::Text("left stick: %f, %f", game->input.gamepad->left_stick.x, game->input.gamepad->left_stick.y);

	ImGui::End();

	ImGui::Render();

	ImDrawData* data = ImGui::GetDrawData();

	data->ScaleClipRects(io.DisplayFramebufferScale);

	GLint last_program; glGetIntegerv(GL_CURRENT_PROGRAM, &last_program);
	GLint last_texture; glGetIntegerv(GL_TEXTURE_BINDING_2D, &last_texture);
	GLint last_active_texture; glGetIntegerv(GL_ACTIVE_TEXTURE, &last_active_texture);
	GLint last_array_buffer; glGetIntegerv(GL_ARRAY_BUFFER_BINDING, &last_array_buffer);
	GLint last_element_array_buffer; glGetIntegerv(GL_ELEMENT_ARRAY_BUFFER_BINDING, &last_element_array_buffer);
	GLint last_vertex_array; glGetIntegerv(GL_VERTEX_ARRAY_BINDING, &last_vertex_array);
	GLint last_blend_src; glGetIntegerv(GL_BLEND_SRC, &last_blend_src);
	GLint last_blend_dst; glGetIntegerv(GL_BLEND_DST, &last_blend_dst);
	GLint last_blend_equation_rgb; glGetIntegerv(GL_BLEND_EQUATION_RGB, &last_blend_equation_rgb);
	GLint last_blend_equation_alpha; glGetIntegerv(GL_BLEND_EQUATION_ALPHA, &last_blend_equation_alpha);
	GLint last_viewport[4]; glGetIntegerv(GL_VIEWPORT, last_viewport);
	GLboolean last_enable_blend = glIsEnabled(GL_BLEND);
	GLboolean last_enable_cull_face = glIsEnabled(GL_CULL_FACE);
	GLboolean last_enable_depth_test = glIsEnabled(GL_DEPTH_TEST);
	GLboolean last_enable_scissor_test = glIsEnabled(GL_SCISSOR_TEST);

	glEnable(GL_BLEND);
	glBlendEquation(GL_FUNC_ADD);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDisable(GL_CULL_FACE);
	glDisable(GL_DEPTH_TEST);
	glEnable(GL_SCISSOR_TEST);
	glActiveTexture(GL_TEXTURE0);

	glUseProgram(dbg->shader.id);
	glUniform1i(g_AttribLocationTex, 0);
	al::gl_shader_set_mat4(&dbg->shader, "projection", game->projection);
	glBindVertexArray(dbg->vao_debug);

	for (int n = 0; n < data->CmdListsCount; n++)
	{
		const ImDrawList* cmd_list = data->CmdLists[n];
		const ImDrawIdx* idx_buffer_offset = 0;

		glBindBuffer(GL_ARRAY_BUFFER, dbg->vbo_debug);

		// #NOTE Changed from GLsizeiptr to GLsizei. Type didn't even exist. Is this fucked?
		glBufferData(GL_ARRAY_BUFFER, (GLsizei)cmd_list->VtxBuffer.size() * sizeof(ImDrawVert), (GLvoid*)&cmd_list->VtxBuffer.front(), GL_STREAM_DRAW);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, dbg->ebo_debug);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, (GLsizei)cmd_list->IdxBuffer.size() * sizeof(ImDrawIdx), (GLvoid*)&cmd_list->IdxBuffer.front(), GL_STREAM_DRAW);

		for (const ImDrawCmd* pcmd = cmd_list->CmdBuffer.begin(); pcmd != cmd_list->CmdBuffer.end(); pcmd++)
		{
			if (pcmd->UserCallback)
			{
				pcmd->UserCallback(cmd_list, pcmd);
			}
			else
			{
				glBindTexture(GL_TEXTURE_2D, (GLuint)(intptr_t)pcmd->TextureId);
				glScissor((int)pcmd->ClipRect.x, (int)(game->client_area.y - pcmd->ClipRect.w), (int)(pcmd->ClipRect.z - pcmd->ClipRect.x), (int)(pcmd->ClipRect.w - pcmd->ClipRect.y));
				glDrawElements(GL_TRIANGLES, (GLsizei)pcmd->ElemCount, sizeof(ImDrawIdx) == 2 ? GL_UNSIGNED_SHORT : GL_UNSIGNED_INT, idx_buffer_offset);
			}
			idx_buffer_offset += pcmd->ElemCount;
		}
	}
	// Restore modified GL state
	glUseProgram(last_program);
	glActiveTexture(last_active_texture);
	glBindTexture(GL_TEXTURE_2D, last_texture);
	glBindVertexArray(last_vertex_array);
	glBindBuffer(GL_ARRAY_BUFFER, last_array_buffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, last_element_array_buffer);
	glBlendEquationSeparate(last_blend_equation_rgb, last_blend_equation_alpha);
	glBlendFunc(last_blend_src, last_blend_dst);
	if (last_enable_blend) glEnable(GL_BLEND); else glDisable(GL_BLEND);
	if (last_enable_cull_face) glEnable(GL_CULL_FACE); else glDisable(GL_CULL_FACE);
	if (last_enable_depth_test) glEnable(GL_DEPTH_TEST); else glDisable(GL_DEPTH_TEST);
	if (last_enable_scissor_test) glEnable(GL_SCISSOR_TEST); else glDisable(GL_SCISSOR_TEST);
	glViewport(last_viewport[0], last_viewport[1], (GLsizei)last_viewport[2], (GLsizei)last_viewport[3]);
}

#endif // AL_IMGUI
