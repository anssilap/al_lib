#ifndef AL_LIB_H
#define AL_LIB_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#ifndef _WIN32
#include <sys/stat.h>
#endif
#include <time.h>

//
// Shorthands for basic datatypes
//
typedef signed char i8;
typedef short i16;
typedef int i32;
typedef long long i64;

typedef unsigned char u8;
typedef unsigned short u16;
typedef unsigned int u32;
typedef unsigned long long u64;

typedef int b32;

typedef float f32;
typedef double f64;

struct _al_BufferHeader
{
    u64 count;
    u64 capacity;
};

//
// Dynamic array
//
#ifndef AL_BUFFER_GROW_FORMULA
#define AL_BUFFER_GROW_FORMULA(x) (2*(x) + 8)
#endif

#define _al_BufferHdr(x)    ((_al_BufferHeader *)(x) - 1)
#define al_BufferCount(x)     (_al_BufferHdr(x)->count)
#define al_BufferCapacity(x)  (_al_BufferHdr(x)->capacity)

#define al_BufferInit(a) do { \
    a = (decltype(a))malloc(sizeof(_al_BufferHeader)); \
    _al_BufferHeader* h = (_al_BufferHeader*)a; \
    h->count = 0; \
    h->capacity = 0; \
    a = (decltype(a))(h+1); \
} while(0)

#define al_BufferFree(a) do { \
    _al_BufferHeader* h = _al_BufferHdr(a); \
    free(h); \
    (a) = NULL; \
} while(0)

//#define alBufferSetCapacity(a, cap) do { \
//	if (a) { \
//		void**al__array_ = void**&(a); \
//	} \
//}

#define _al_BufferGrow(a, min_cap) do { \
    _al_BufferHeader* h = _al_BufferHdr(a); \
    u64 new_cap = AL_BUFFER_GROW_FORMULA(al_BufferCapacity(a)); \
    if (new_cap < (min_cap)) { \
        new_cap = (min_cap); \
    } \
    a = (decltype(a))realloc(h, sizeof(_al_BufferHeader) + sizeof(a[0]) * new_cap); \
    h = (_al_BufferHeader*)a; \
    h->capacity = new_cap; \
    a = (decltype(a))(h+1); \
} while(0)

#define al_BufferClear(a) do { \
    _al_BufferHeader* h = (_al_BufferHeader*)a; \
    h->count = 0; \
} while (0)

#define al_BufferAdd(a, e) do { \
    if (al_BufferCapacity(a) <= al_BufferCount(a)) { \
        _al_BufferGrow(a, al_BufferCapacity(a)+1); \
    } \
    (a)[al_BufferCount(a)++] = (e); \
} while(0)

//
// Copies elements to buffer
// Usage: alBufferAddv(a, e, count)
// a = pointer to buffer
// e = array of data
// count = number of elements to copy
//
#define al_BufferAddv(a, e, count) do { \
    al_Assert(sizeof(a[0]) == sizeof(e[0])); \
    if (al_BufferCapacity(a) < al_BufferCount(a) + count) { \
        _al_BufferGrow(a, al_BufferCount(a) + count); \
    } \
    memcpy(&(a)[al_BufferCount(a)], (e), count * sizeof(a[0])); \
    al_BufferCount(a) += count; \
} while (0)

#define al_ArrayCount(arr) (sizeof(arr) / sizeof((arr)[0]))

// 
// File operations - definitions
//
void al_FileAppend(char* filename, char* s, ...);
size_t al_FileLen(FILE* f);
void* al_FileRead(char* filename, size_t* length);
void* al_FileRead(const char* filename, size_t* length);
inline b32 al_FileExists(const char* filename);
#endif // AL_LIB_H

// 
// ***************************
// ***** IMPLEMENTATIONS *****
// ***************************
//
#ifdef AL_LIB_IMPLEMENTATION
#undef AL_LIB_IMPLEMENTATION
//
// File operations - implementation
//
inline void al_FileAppend(char* filename, char* s, ...)
{
#ifdef _MSC_VER
#pragma warning(disable: 4996)
#endif
    FILE *f;
    fopen_s(&f, filename, "a");
    if (f)
    {
        va_list a;
        va_start(a, s);
        vfprintf(f, s, a);
        va_end(a);
        fputs("\n", f);
        fclose(f);
    }
}

inline size_t al_FileLen(FILE* f)
{
    size_t len, pos;
    pos = ftell(f);
    fseek(f, 0, SEEK_END);
    len = ftell(f);
    fseek(f, (long)pos, SEEK_SET);
    return len;
}

inline void* al_FileRead(char* filename, size_t* length)
{
    FILE *f;
    fopen_s(&f, filename, "rb");
    char *buffer;

    size_t len1, len2;
    if (!f)
        return NULL;

    len1 = al_FileLen(f);
    buffer = (char*)malloc(len1 + 2);
    len2 = fread(buffer, 1, len1, f);
    if (len2 == len1)
    {
        if (length)
            *length = len1;
        buffer[len1] = 0;
    }
    else
    {
        free(buffer);
        buffer = NULL;
    }
    fclose(f);
    return buffer;
}

inline void* al_FileRead(const char* filename, size_t* length) {
    FILE* f;
    fopen_s(&f, filename, "rb");
    char* buffer;

    size_t len1, len2;
    if (!f)
        return NULL;

    len1 = al_FileLen(f);
    buffer = (char*)malloc(len1 + 2);
    len2 = fread(buffer, 1, len1, f);
    if (len2 == len1)
    {
        if (length)
            *length = len1;
        buffer[len1] = 0;
    }
    else
    {
        free(buffer);
        buffer = NULL;
    }
    fclose(f);
    return buffer;
}

inline b32 al_FileExists(const char* filename)
{
#ifdef _WIN32
    if (FILE* file = fopen(filename, "r"))
    {
        fclose(file);
        return true;
    }
    else
    {
        return false;
    }
#else
    struct stat buffer;
    return (stat(filename, &buffer) == 0);
#endif
}
#endif // AL_LIB_IMPLEMENTATION
