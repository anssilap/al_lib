#ifndef AL_INPUT
#define AL_INPUT

#include <al_lib.h>
#include <math.h>

enum {
    al_INPUT_MAX_KEYS = 256,
    al_INPUT_MAX_TEXT = 1024,
};

enum {
    al_KEY_BACKSPACE = 0x08,
    al_KEY_TAB = 0x09,
    al_KEY_SHIFT = 0x10,
    al_KEY_CTRL = 0x11,
    al_KEY_ALT = 0x12,
    al_KEY_PAUSE = 0x13,
    al_KEY_CAPSLOCK = 0x14,
    al_KEY_ESCAPE = 0x1B,
    al_KEY_SPACE = 0x20,
    al_KEY_PAGEUP = 0x21,
    al_KEY_PAGEDOWN = 0x22,
    al_KEY_END = 0x23,
    al_KEY_HOME = 0x24,
    al_KEY_LEFT = 0x25,
    al_KEY_UP = 0x26,
    al_KEY_RIGHT = 0x27,
    al_KEY_DOWN = 0x28,
    al_KEY_RETURN = 0x0D,
    al_KEY_INSERT = 0x2D,
    al_KEY_DELETE = 0x2E,
    al_KEY_A = 0x41,
    al_KEY_B = 0x42,
    al_KEY_C = 0x43,
    al_KEY_D = 0x44,
    al_KEY_E = 0x45,
    al_KEY_F = 0x46,
    al_KEY_G = 0x47,
    al_KEY_H = 0x48,
    al_KEY_I = 0x49,
    al_KEY_J = 0x4A,
    al_KEY_K = 0x4B,
    al_KEY_L = 0x4C,
    al_KEY_M = 0x4D,
    al_KEY_N = 0x4E,
    al_KEY_O = 0x4F,
    al_KEY_P = 0x50,
    al_KEY_Q = 0x51,
    al_KEY_R = 0x52,
    al_KEY_S = 0x53,
    al_KEY_T = 0x54,
    al_KEY_U = 0x55,
    al_KEY_V = 0x56,
    al_KEY_W = 0x57,
    al_KEY_X = 0x58,
    al_KEY_Y = 0x59,
    al_KEY_Z = 0x5A,
    al_KEY_NUMPAD0 = 0x60,
    al_KEY_NUMPAD1 = 0x61,
    al_KEY_NUMPAD2 = 0x62,
    al_KEY_NUMPAD3 = 0x63,
    al_KEY_NUMPAD4 = 0x64,
    al_KEY_NUMPAD5 = 0x65,
    al_KEY_NUMPAD6 = 0x66,
    al_KEY_NUMPAD7 = 0x67,
    al_KEY_NUMPAD8 = 0x68,
    al_KEY_NUMPAD9 = 0x69,
    al_KEY_MULTIPLY = 0x6A,
    al_KEY_ADD = 0x6B,
    al_KEY_SEPARATOR = 0x6C,
    al_KEY_SUBSTRACT = 0x6D,
    al_KEY_DECIMAL = 0x6E,
    al_KEY_DIVIDE = 0x6F,
    al_KEY_F1 = 0x70,
    al_KEY_F2 = 0x71,
    al_KEY_F3 = 0x72,
    al_KEY_F4 = 0x73,
    al_KEY_F5 = 0x74,
    al_KEY_F6 = 0x75,
    al_KEY_F7 = 0x76,
    al_KEY_F8 = 0x77,
    al_KEY_F9 = 0x78,
    al_KEY_F10 = 0x79,
    al_KEY_F11 = 0x7A,
    al_KEY_F12 = 0x7B,
    al_KEY_SECTION = 0xdc,
};

struct al_InputAnalogButton
{
    f32 value;
    f32 threshold;
    b32 down;
    b32 pressed;
    b32 released;
};

struct al_InputButton
{
    b32 down;
    b32 pressed;
    b32 released;
};

struct al_InputStick
{
    f32 threshold;
    f32 x;
    f32 y;
};

struct al_InputMouse
{
    al_InputButton left_button;
    al_InputButton right_button;
    i32 wheel;
    i32 delta_wheel;
    f32 position_x;
    f32 position_y;
    f32 delta_pos_x;
    f32 delta_pos_y;
    f32 screen_position_x;
    f32 screen_position_y;
    f32 sensitivity;
    b32 invert_y;
};

struct al_InputGamepad
{
    b32 connected;
    al_InputButton a_button;
    al_InputButton b_button;
    al_InputButton x_button;
    al_InputButton y_button;

    al_InputStick left_stick;
    al_InputStick right_stick;
    al_InputButton left_stick_button;
    al_InputButton right_stick_button;
    al_InputAnalogButton left_trigger;
    al_InputAnalogButton right_trigger;
    al_InputButton left_shoulder_button;
    al_InputButton right_shoulder_button;
    al_InputButton up_button;
    al_InputButton down_button;
    al_InputButton left_button;
    al_InputButton right_button;
    al_InputButton start_button;
    al_InputButton back_button;
};

struct al_Input
{
    /// Array of keyboard key states
    al_InputButton keyboard[al_INPUT_MAX_KEYS];
    /// Gamepad state data
    al_InputGamepad gamepad[4];
    /// Mouse state data, buttons need to be reset manually after handling pressed and released
    al_InputMouse mouse;
};
#endif // AL_INPUT

#ifdef AL_INPUT_IMPLEMENTATION
#undef AL_INPUT_IMPLEMENTATION
void al_InputPullAnalogButton(al_InputAnalogButton* button, f32 value)
{
    button->value = value;
    b32 was_down = button->down;
    button->down = (value > button->threshold);
    button->pressed = !was_down && button->down;
    button->released = was_down && !button->down;
}

void al_InputPullButton(al_InputButton* button, b32 down)
{
    b32 was_down = button->down;
    button->pressed = !was_down && down;
    button->released = was_down && !down;
    button->down = down;
}

void al_InputPullStick(al_InputStick* stick, f32 x, f32 y)
{
    if (fabs(x) <= stick->threshold)
        x = 0.0f;
    stick->x = x;

    if (fabs(y) <= stick->threshold)
        y = 0.0f;
    stick->y = y;
}

void al_InputResetButton(al_InputButton* button)
{
    button->pressed = false;
    //button->down = false;
    button->released = false;
}
#endif // AL_INPUT_IMPLEMENTATION
