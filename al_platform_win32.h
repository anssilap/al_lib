#pragma once

#ifndef AL_PLATFORM_WIN32
#define AL_PLATFORM_WIN32

#include <windows.h>
#include <Xinput.h>
#include <stdio.h>

typedef DWORD(WINAPI* XINPUTGETSTATE)(DWORD dwUserIndex, XINPUT_STATE* pState);

struct al_Win32Data
{
    HWND window;
    HDC dc;
    WNDPROC event_handler;
    DWORD window_style;
    void* fiber_main;
    void* fiber_message;
    HGLRC opengl_context;
    XINPUTGETSTATE xinput_getstate;
};

static void CALLBACK al_Win32MessageFiberProc(al_Win32Data* win32)
{
    for (;;)
    {
        MSG message;
        while (PeekMessage(&message, 0, 0, 0, PM_REMOVE))
        {
            TranslateMessage(&message);
            DispatchMessage(&message);
        }
        SwitchToFiber(win32->fiber_main);
    }
}

void al_Win32InitMessageProcessing(al_Win32Data* win32)
{
    win32->dc = GetWindowDC(win32->window);
    al_Assert(win32->dc);
    al_Assert(win32->event_handler);

    win32->fiber_main = ConvertThreadToFiber(0);
    al_Assert(win32->fiber_main);
    win32->fiber_message = CreateFiber(0, (PFIBER_START_ROUTINE)al_Win32MessageFiberProc, win32);
    al_Assert(win32->fiber_message);
}

void al_Win32ProcessMessages(al_Win32Data* win32)
{
    SwitchToFiber(win32->fiber_message);
}

void al_SetWindowTitleFormatted(HWND window, char* format, ...)
{
    va_list v;
    va_start(v, format);
    char buffer[512];
    _vsnprintf_s(buffer, sizeof(buffer) / sizeof(char), format, v);
#ifdef UNICODE
    BSTR output;
    int length = lstrlenA(buffer);
    int length_w = MultiByteToWideChar(CP_ACP, 0, buffer, length, 0, 0);
    if (length_w > 0)
    {
        output = SysAllocStringLen(0, length_w);
        MultiByteToWideChar(CP_ACP, 0, buffer, length, output, length_w);
        SetWindowTextW(window, output);
        SysFreeString(output);
    }
#else
    SetWindowText(window, buffer);
#endif
}

HWND al_Win32CreateWindow(char* window_title = "al_engine window",
    WNDPROC event_handler = nullptr,
    int client_width = CW_USEDEFAULT, int client_height = CW_USEDEFAULT,
    int window_pos_x = CW_USEDEFAULT, int window_pos_y = CW_USEDEFAULT,
    DWORD window_style = WS_OVERLAPPEDWINDOW, DWORD window_style_ex = WS_EX_TRANSPARENT,
    HINSTANCE instance = NULL)
{
    HWND window = nullptr;
    if (event_handler == nullptr)
        return window;

    WNDCLASSEX wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);
    wcex.style = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc = event_handler;
    wcex.cbClsExtra = 0;
    wcex.cbWndExtra = 0;
    wcex.hInstance = instance;
    wcex.hIcon = LoadIcon(instance, IDI_APPLICATION);
    wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
    wcex.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
    wcex.lpszMenuName = NULL;
#ifdef UNICODE
    LPCWSTR window_class = L"alWindowClass";
    wcex.lpszClassName = window_class;
#else
    const char* window_class = "alWindowClass";
    wcex.lpszClassName = window_class;
#endif
    wcex.hIconSm = LoadIcon(instance, IDI_APPLICATION);
    if (!RegisterClassEx(&wcex))
        return nullptr;
        
    int window_width = client_width;
    int window_height = client_height;

    if (window_width != CW_USEDEFAULT && window_height != CW_USEDEFAULT) {
        RECT windowRect = { 0, 0, window_width, window_height };
        AdjustWindowRect(&windowRect, window_style, false);

        window_width = windowRect.right - windowRect.left;
        window_height = windowRect.bottom - windowRect.top;
    }

#ifdef UNICODE
    BSTR output;
    int length = lstrlenA(window_title);
    int length_w = MultiByteToWideChar(CP_ACP, 0, window_title, length, 0, 0);
    if (length_w > 0)
    {
        output = SysAllocStringLen(0, length_w);
        MultiByteToWideChar(CP_ACP, 0, window_title, length, output, length_w);
        SysFreeString(output);
    }
    else
    {
        output = SysAllocStringLen(0, length_w);
        MultiByteToWideChar(CP_ACP, 0, "", length, output, length_w);
    }

    window = CreateWindowEx(window_style_ex, window_class, output,
        window_style, window_pos_x, window_pos_y, window_width, window_height,
        NULL, NULL, instance, NULL);
#else
    window = CreateWindowEx(window_style_ex, window_class, window_title,
        window_style, window_pos_x, window_pos_y, window_width, window_height,
        NULL, NULL, instance, NULL);
#endif
    if (!window)
    {
    #ifdef UNICODE
        MessageBox(window, (LPCWSTR)L"Couldn't create Win32 window",
            (LPCWSTR)L"Error", MB_ICONERROR | MB_OK);
    #else
        MessageBox(window, "Couldn't create Win32 window",
            "Error", MB_ICONERROR | MB_OK);
    #endif
        __debugbreak();
    }

    ShowWindow(window, SW_SHOW);
    UpdateWindow(window);

    return window;
}

#endif // AL_PLATFORM_WIN32
